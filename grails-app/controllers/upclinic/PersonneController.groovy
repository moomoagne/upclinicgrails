package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PersonneController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Personne.list(params), model:[personneCount: Personne.count()]
    }

    def show(Personne personne) {
        respond personne
    }

    def create() {
        respond new Personne(params)
    }

    @Transactional
    def save(Personne personne) {
        if (personne == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (personne.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond personne.errors, view:'create'
            return
        }

        personne.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'personne.label', default: 'Personne'), personne.id])
                redirect personne
            }
            '*' { respond personne, [status: CREATED] }
        }
    }

    def edit(Personne personne) {
        respond personne
    }

    @Transactional
    def update(Personne personne) {
        if (personne == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (personne.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond personne.errors, view:'edit'
            return
        }

        personne.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'personne.label', default: 'Personne'), personne.id])
                redirect personne
            }
            '*'{ respond personne, [status: OK] }
        }
    }

    @Transactional
    def delete(Personne personne) {

        if (personne == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        personne.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'personne.label', default: 'Personne'), personne.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'personne.label', default: 'Personne'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
