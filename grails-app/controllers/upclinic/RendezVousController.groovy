package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RendezVousController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RendezVous.list(params), model:[rendezVousCount: RendezVous.count()]
    }

    def show(RendezVous rendezVous) {
        respond rendezVous
    }

    def create() {
        respond new RendezVous(params)
    }

    @Transactional
    def save(RendezVous rendezVous) {
        if (rendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (rendezVous.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond rendezVous.errors, view:'create'
            return
        }

        rendezVous.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'rendezVous.label', default: 'RendezVous'), rendezVous.id])
                redirect rendezVous
            }
            '*' { respond rendezVous, [status: CREATED] }
        }
    }

    def edit(RendezVous rendezVous) {
        respond rendezVous
    }

    @Transactional
    def update(RendezVous rendezVous) {
        if (rendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (rendezVous.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond rendezVous.errors, view:'edit'
            return
        }

        rendezVous.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'rendezVous.label', default: 'RendezVous'), rendezVous.id])
                redirect rendezVous
            }
            '*'{ respond rendezVous, [status: OK] }
        }
    }

    @Transactional
    def delete(RendezVous rendezVous) {

        if (rendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        rendezVous.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'rendezVous.label', default: 'RendezVous'), rendezVous.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rendezVous.label', default: 'RendezVous'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
