package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MedecinController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Medecin.list(params), model:[medecinCount: Medecin.count()]
    }

    def show(Medecin medecin) {
        respond medecin
    }

    def create() {
        respond new Medecin(params)
    }

    @Transactional
    def save(Medecin medecin) {
        if (medecin == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (medecin.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond medecin.errors, view:'create'
            return
        }

        medecin.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'medecin.label', default: 'Medecin'), medecin.id])
                redirect medecin
            }
            '*' { respond medecin, [status: CREATED] }
        }
    }

    def edit(Medecin medecin) {
        respond medecin
    }

    @Transactional
    def update(Medecin medecin) {
        if (medecin == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (medecin.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond medecin.errors, view:'edit'
            return
        }

        medecin.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'medecin.label', default: 'Medecin'), medecin.id])
                redirect medecin
            }
            '*'{ respond medecin, [status: OK] }
        }
    }

    @Transactional
    def delete(Medecin medecin) {

        if (medecin == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        medecin.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'medecin.label', default: 'Medecin'), medecin.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'medecin.label', default: 'Medecin'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
