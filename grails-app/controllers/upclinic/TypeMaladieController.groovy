package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TypeMaladieController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TypeMaladie.list(params), model:[typeMaladieCount: TypeMaladie.count()]
    }

    def show(TypeMaladie typeMaladie) {
        respond typeMaladie
    }

    def create() {
        respond new TypeMaladie(params)
    }

    @Transactional
    def save(TypeMaladie typeMaladie) {
        if (typeMaladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (typeMaladie.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond typeMaladie.errors, view:'create'
            return
        }

        typeMaladie.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'typeMaladie.label', default: 'TypeMaladie'), typeMaladie.id])
                redirect typeMaladie
            }
            '*' { respond typeMaladie, [status: CREATED] }
        }
    }

    def edit(TypeMaladie typeMaladie) {
        respond typeMaladie
    }

    @Transactional
    def update(TypeMaladie typeMaladie) {
        if (typeMaladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (typeMaladie.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond typeMaladie.errors, view:'edit'
            return
        }

        typeMaladie.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'typeMaladie.label', default: 'TypeMaladie'), typeMaladie.id])
                redirect typeMaladie
            }
            '*'{ respond typeMaladie, [status: OK] }
        }
    }

    @Transactional
    def delete(TypeMaladie typeMaladie) {

        if (typeMaladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        typeMaladie.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'typeMaladie.label', default: 'TypeMaladie'), typeMaladie.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeMaladie.label', default: 'TypeMaladie'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
