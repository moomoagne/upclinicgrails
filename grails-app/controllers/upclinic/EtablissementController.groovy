package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EtablissementController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

/*    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Etablissement.list(params), model:[etablissementCount: Etablissement.count()]
    }*/
    def index(){
        def lists = Etablissement.list()
        [etablissementList: lists]
    }

    def show(Etablissement etablissement) {
        respond etablissement
    }

    def create() {
        respond new Etablissement(params)
    }

    @Transactional
    def save(Etablissement etablissement) {
        if (etablissement == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (etablissement.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond etablissement.errors, view:'create'
            return
        }

        etablissement.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'etablissement.label', default: 'Etablissement'), etablissement.id])
                redirect etablissement
            }
            '*' { respond etablissement, [status: CREATED] }
        }
    }

    def edit(Etablissement etablissement) {
        respond etablissement
    }

    @Transactional
    def update(Etablissement etablissement) {
        if (etablissement == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (etablissement.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond etablissement.errors, view:'edit'
            return
        }

        etablissement.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'etablissement.label', default: 'Etablissement'), etablissement.id])
                redirect etablissement
            }
            '*'{ respond etablissement, [status: OK] }
        }
    }

    @Transactional
    def delete(Etablissement etablissement) {

        if (etablissement == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        etablissement.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'etablissement.label', default: 'Etablissement'), etablissement.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'etablissement.label', default: 'Etablissement'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
