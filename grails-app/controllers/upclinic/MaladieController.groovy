package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaladieController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Maladie.list(params), model:[maladieCount: Maladie.count()]
    }

    def show(Maladie maladie) {
        respond maladie
    }

    def create() {
        respond new Maladie(params)
    }

    @Transactional
    def save(Maladie maladie) {

        log.info("dbdbdbdh : ${params}")

        if (maladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (maladie.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond maladie.errors, view:'create'
            return
        }

        maladie.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'maladie.label', default: 'Maladie'), maladie.id])
                redirect maladie
            }
            '*' { respond maladie, [status: CREATED] }
        }
    }

    def edit(Maladie maladie) {
        respond maladie
    }

    @Transactional
    def update(Maladie maladie) {
        if (maladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (maladie.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond maladie.errors, view:'edit'
            return
        }

        maladie.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'maladie.label', default: 'Maladie'), maladie.id])
                redirect maladie
            }
            '*'{ respond maladie, [status: OK] }
        }
    }

    @Transactional
    def delete(Maladie maladie) {

        if (maladie == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        maladie.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'maladie.label', default: 'Maladie'), maladie.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'maladie.label', default: 'Maladie'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
