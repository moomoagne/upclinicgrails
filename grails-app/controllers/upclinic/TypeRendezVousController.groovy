package upclinic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TypeRendezVousController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TypeRendezVous.list(params), model:[typeRendezVousCount: TypeRendezVous.count()]
    }

    def show(TypeRendezVous typeRendezVous) {
        respond typeRendezVous
    }

    def create() {
        respond new TypeRendezVous(params)
    }

    @Transactional
    def save(TypeRendezVous typeRendezVous) {
        if (typeRendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (typeRendezVous.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond typeRendezVous.errors, view:'create'
            return
        }

        typeRendezVous.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'typeRendezVous.label', default: 'TypeRendezVous'), typeRendezVous.id])
                redirect typeRendezVous
            }
            '*' { respond typeRendezVous, [status: CREATED] }
        }
    }

    def edit(TypeRendezVous typeRendezVous) {
        respond typeRendezVous
    }

    @Transactional
    def update(TypeRendezVous typeRendezVous) {
        if (typeRendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (typeRendezVous.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond typeRendezVous.errors, view:'edit'
            return
        }

        typeRendezVous.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'typeRendezVous.label', default: 'TypeRendezVous'), typeRendezVous.id])
                redirect typeRendezVous
            }
            '*'{ respond typeRendezVous, [status: OK] }
        }
    }

    @Transactional
    def delete(TypeRendezVous typeRendezVous) {

        if (typeRendezVous == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        typeRendezVous.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'typeRendezVous.label', default: 'TypeRendezVous'), typeRendezVous.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeRendezVous.label', default: 'TypeRendezVous'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
