package upclinic

class TypeRendezVous {

    String code
    String intitule

    static constraints = {
        code nullable: false, blank: false, unique: true
        intitule inList: ["premier_rv", "rv_apres_consultation"]
    }
}