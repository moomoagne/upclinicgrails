package upclinic

class TypeMaladie {

    String code
    String intitule
    String description

    static constraints = {
        code nullable: false, unique: true, blank: false
        intitule blank: false, nullable: false
        description blank: false, type:'text'
    }
}