package upclinic

class Personne {

    String nom
    String prenom
    String sexe
    String adresse
    Integer tel
    String email
    Integer age

    static constraints = {
        nom nullable: false, blank: false
        prenom nullable: false, blank: false
        sexe inList: ["Homme", "Femme"], blank: false
        adresse nullable: false ,blank: false
        tel nullable: false ,blank: false
        email email: true, nullable: true, unique: true
        age nullable: false
    }

}
