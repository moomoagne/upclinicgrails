package upclinic

class RendezVous {

    Date date_rv
    Date date_rv_update
    TypeRendezVous type_rv
    String description
    Medecin id_medecin
    Patient id_patient
    boolean effectuer

    static constraints = {
        description blank: false, nullable: false, type:'text'
        effectuer defaultValue: false
    }
}
