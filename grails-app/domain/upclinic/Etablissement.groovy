package upclinic


class Etablissement {

    String nom_etat
    String adresse_eta
    String email
    String site
    byte[] logo
    String logoContentType
    Integer tel
    String slogan

    static constraints = {
        nom_etat blank: false, nullable: false, unique: true
        tel nullable: false , blank: false
        site nullable: true, blank: false
        logo(nullable:true, maxSize:1000000)
        logoContentType nullable: true
        slogan nullable: true, blank: false
        adresse_eta blank: false, nullable: false
        email email: true, blank: false, unique: true
    }

    static mapping = {
        logo column: 'logo', sqlType: 'longblog'
    }

    String toString(){
        return "${nom_etat}"
    }
}
