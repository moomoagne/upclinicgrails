package upclinic

class Patient extends Personne{

    String profession

    static constraints = {
        profession blank: false
    }
}
