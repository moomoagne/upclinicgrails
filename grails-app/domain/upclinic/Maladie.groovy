package upclinic

class Maladie {

    String code
    String intitule
    TypeMaladie type_maladie
    String description

    static constraints = {
        code nullable: false, blank: false, unique: true
        intitule nullable: false, blank: false
        type_maladie nullable: false
        description nullable: true, type: 'text'
    }

    String toString(){
        return "${intitule}"
    }
}
