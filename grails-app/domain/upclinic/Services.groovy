package upclinic

class Services {

    String code
    String intitule
    String description

    static constraints = {
        code unique: true , nullable: false, blank:false
        intitule nullable: false, blank: false
        description nullable: true, type:'text'
    }
}
